package org.videolan.podcast

import android.app.Application
import org.videolan.podcast.core.room.PodcastDatabase
import org.videolan.podcast.core.room.repository.DataRepository
import org.videolan.podcast.core.update.UpdateWorker

class PodcastApp : Application() {

    override fun onCreate() {
        super.onCreate()
        database = PodcastDatabase.getInstance(this, appExecutors)
        repository = DataRepository.getInstance(database)

        UpdateWorker.launch(this)


    }

    companion object {

        lateinit var database: PodcastDatabase

        lateinit var repository: DataRepository
        val appExecutors: AppExecutors = AppExecutors()
    }


}
