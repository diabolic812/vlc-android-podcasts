package org.videolan.podcast

import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.text.InputType
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatEditText
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import be.ceau.opml.OpmlParser
import be.ceau.opml.entity.Outline
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import org.videolan.podcast.core.room.entities.PodcastEntity
import org.videolan.podcast.core.update.UpdateManager
import org.videolan.podcast.viewmodel.PodcastViewModel
import org.videolan.vlc.extensions.api.PermissionHelper
import org.videolan.vlc.extensions.api.PermissionStatus
import org.videolan.vlc.extensions.api.VLCPermission
import org.videolan.vlc.extensions.api.VLCPermissionRequest
import org.videolan.vlc.extensions.api.tools.Helpers

private const val PODCAST_ADD_FAIL = 0
private const val PODCAST_ADD_SUCCESS = 1
private const val PODCAST_REMOVE_SUCCESS = 2
private const val PODCAST_REMOVE_ALL_SUCCESS = 3
private const val PODCAST_ALREADY_ADDED = 4
private const val REQUEST_OPEN_DOCUMENT = 2


class MainActivity : AppCompatActivity(), PermissionHelper.OnVLCPermissionResult {
    private var alertDialog: AlertDialog? = null
    private lateinit var adapter: SubscriptionAdapter

    private lateinit var list: RecyclerView
    private var mLayoutManager: RecyclerView.LayoutManager? = null

    private val handler: Handler = object : Handler() {
        override fun handleMessage(msg: Message) {
            when (msg.what) {
                PODCAST_ADD_FAIL -> Snackbar.make(window.decorView, (msg.obj as Exception).message as CharSequence, Snackbar.LENGTH_LONG).show()
                PODCAST_ADD_SUCCESS -> {
                }
                PODCAST_REMOVE_SUCCESS -> {
                    Snackbar.make(window.decorView, R.string.removed_snackbar_title, Snackbar.LENGTH_SHORT).show()
                    adapter.notifyDataSetChanged()
                }
                PODCAST_REMOVE_ALL_SUCCESS -> {
                    Snackbar.make(window.decorView, R.string.removed_all_snackbar_title, Snackbar.LENGTH_SHORT).show()
                    adapter.notifyDataSetChanged()
                }
                PODCAST_ALREADY_ADDED -> Snackbar.make(window.decorView, R.string.already_added_snackbar_title, Snackbar.LENGTH_SHORT).show()
            }//                    updateList();
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (!Helpers.checkVlc(this)) {
            finish()
            return
        }
        setContentView(R.layout.activity_main)
        val toolbar = findViewById<View>(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)

        list = findViewById<View>(R.id.subscriptions_list) as RecyclerView
        mLayoutManager = LinearLayoutManager(this)
        list.layoutManager = mLayoutManager
        adapter = SubscriptionAdapter(this)
        list.adapter = adapter
        val fab = findViewById<View>(R.id.fab) as FloatingActionButton
        fab.setOnClickListener { showAddPodcastDialog() }

        val readPermission = VLCPermissionRequest()
        readPermission.permission = VLCPermission.READ_STATUS
        readPermission.reason = "The read status is needed to avoid showing already read episodes"

        val progressPermission = VLCPermissionRequest()
        progressPermission.permission = VLCPermission.PROGRESS_STATUS
        progressPermission.reason = "The progress is used to display a progress bar in the app"
        val permissionHelper = PermissionHelper.newInstance(this)
        permissionHelper.askPermissions(readPermission, progressPermission)

        val viewModel = PodcastViewModel(application)
        viewModel.allPodcasts.observe(this, Observer { podcastEntries ->
            adapter.clear()
            adapter.addAll(podcastEntries)
            adapter.notifyDataSetChanged()
        })

    }

    override fun onStart() {
        super.onStart()
    }

    public override fun onStop() {
        super.onStop()
        if (alertDialog != null && alertDialog!!.isShowing)
            alertDialog!!.dismiss()
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent mActivity in AndroidManifest.xml.

        when (item.itemId) {
            R.id.action_clear -> removeAllPodcastDialog()
            R.id.action_opml -> loadOPML()
            R.id.action_refresh -> {
                PodcastApp.appExecutors.diskIO().execute {
                    UpdateManager.updateAll(applicationContext)
                }
            }
//           R.id.action_settings -> SettingsActivity.start(this)
        }




        return super.onOptionsItemSelected(item)
    }

    private fun loadOPML() {
        val intent = Intent(Intent.ACTION_OPEN_DOCUMENT)

        // Filter to only show results that can be "opened", such as a
        // file (as opposed to a list of contacts or timezones)
        intent.addCategory(Intent.CATEGORY_OPENABLE)

        // Filter to show only images, using the image MIME data rType.
        // If one wanted to search for ogg vorbis files, the rType would be "audio/ogg".
        // To search for all documents available via installed storage providers,
        // it would be "*/*".
        intent.type = "*/*"

//        val mimetypes = arrayOf("text/xml", "application/xml")
//        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimetypes)

        startActivityForResult(intent, REQUEST_OPEN_DOCUMENT)

    }

    fun removePodcastDialog(item: PodcastEntity) {
        val context = this
        val builder = AlertDialog.Builder(context)
        builder.setTitle(R.string.remove_dialog_title)
        builder.setNegativeButton(android.R.string.cancel, DialogInterface.OnClickListener { dialogInterface, which -> return@OnClickListener })
        builder.setPositiveButton(android.R.string.ok) { dialog, which -> removePodcast(item) }
        alertDialog = builder.show()
    }

    private fun removeAllPodcastDialog() {
        val context = this
        val builder = AlertDialog.Builder(context)
        builder.setTitle(R.string.remove_all_dialog_title)
        builder.setNegativeButton(android.R.string.cancel, DialogInterface.OnClickListener { dialogInterface, which -> return@OnClickListener })
        builder.setPositiveButton(android.R.string.ok) { dialog, which ->
            Thread(Runnable {
                for (item in adapter.items)
                    removePodcast(item.podcast)
                adapter.clear()
                handler.obtainMessage(PODCAST_REMOVE_ALL_SUCCESS).sendToTarget()
            }).start()
        }
        alertDialog = builder.show()
    }

    private fun removePodcast(item: PodcastEntity) {


        PodcastApp.appExecutors.diskIO().execute { PodcastApp.database.podcastDao().delete(item) }

    }


    fun showAddPodcastDialog() {
        val context = this
        val builder = AlertDialog.Builder(context)
        val input = AppCompatEditText(context)
        input.inputType = InputType.TYPE_TEXT_VARIATION_URI
        builder.setTitle("Add a new podcast")
        builder.setMessage("write or paste here your podcast address")
        builder.setView(input)
        builder.setNegativeButton(android.R.string.cancel, DialogInterface.OnClickListener { dialogInterface, which -> return@OnClickListener })
        builder.setPositiveButton(android.R.string.ok) { dialog, which -> addPodcast(input) }
        alertDialog = builder.show()
    }

    private fun addPodcast(input: AppCompatEditText) {
        PodcastApp.appExecutors.diskIO().execute(Runnable {
            try {
                val link = input.text!!.toString().trim { it <= ' ' }

                for (link1 in adapter.items)
                    if (link == link1.podcast.link) {
                        handler.obtainMessage(PODCAST_ALREADY_ADDED).sendToTarget()
                        return@Runnable
                    }


                val podcastEntry = PodcastEntity(
                        link, "", "", "", "", null
                )
                PodcastApp.database.podcastDao().insert(podcastEntry)
                val poscast = PodcastApp.database.podcastsWithEpisodeDao().loadPodcastWithEpisodes(link)
                UpdateManager.updatePodcast(this, poscast)


                handler.obtainMessage(PODCAST_ADD_SUCCESS).sendToTarget()
            } catch (e: Exception) {
                handler.obtainMessage(PODCAST_ADD_FAIL, e).sendToTarget()
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            REQUEST_OPEN_DOCUMENT -> {

                val snackbar = Snackbar.make(list, R.string.importing, Snackbar.LENGTH_INDEFINITE)
                snackbar.show()


                PodcastApp.appExecutors.diskIO().execute {
                    val uri: Uri? = data?.data
                    if (uri != null) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                            contentResolver.takePersistableUriPermission(uri, Intent.FLAG_GRANT_READ_URI_PERMISSION or Intent.FLAG_GRANT_WRITE_URI_PERMISSION)
                        }
                        val inputStream = contentResolver.openInputStream(uri)


//                        val opmlInternalFile = File("$cacheDir/import.opml")
//                        FileUtils.writeStreamToFile(inputStream, opmlInternalFile)



                        val opml = OpmlParser().parse(inputStream);



                        val podcastEntities = ArrayList<PodcastEntity>()
                        getOutlines(opml.body.outlines, podcastEntities)

                        for (podcastEntity in podcastEntities) {
                            PodcastApp.database.podcastDao().insert(podcastEntity)
                        }


                    }

                    UpdateManager.updateAll(this)

                    PodcastApp.appExecutors.mainThread().execute {
                        snackbar.dismiss()

                    }

                }


            }

        }
    }

    private fun getOutlines(outlines: MutableList<Outline>, podcastEntities: ArrayList<PodcastEntity>) {
        for (outline in outlines) {
            outline.attributes["xmlUrl"]?.let {
                podcastEntities.add(PodcastEntity(it, "", "", "", "", null))
            }
            if (outline.subElements.isNotEmpty()) {
                getOutlines(outline.subElements, podcastEntities)
            }
        }
    }


    override fun onPermissionResult(permission: VLCPermission, status: PermissionStatus) {
        when (status) {
            PermissionStatus.GRANTED -> {
                //            Snackbar.make(mTitle, permission.getKey()+" granted", Snackbar.LENGTH_LONG).show();
            }
            PermissionStatus.DENIED_MANIFEST -> {
                //            Snackbar.make(mTitle, permission.getKey()+" no permission in manifest", Snackbar.LENGTH_LONG).show();
            }
            PermissionStatus.DENIED -> {
                //            Snackbar.make(mTitle, permission.getKey()+" denied", Snackbar.LENGTH_LONG).show();
            }
            else -> {
                //            Snackbar.make(mTitle, permission.getKey()+" unknown", Snackbar.LENGTH_LONG).show();
            }
        }
    }

    override fun onVLCNotInstalled() {
        Snackbar.make(list, "VLC is not installed!", Snackbar.LENGTH_LONG).show()
    }

}
