package org.videolan.podcast.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import org.videolan.podcast.PodcastApp
import org.videolan.podcast.core.room.entities.PodcastWithEpisodes


class PodcastViewModel(application: Application) : AndroidViewModel(application) {

    val allPodcasts: LiveData<List<PodcastWithEpisodes>> = PodcastApp.repository.podcastsWithEpisodes





}