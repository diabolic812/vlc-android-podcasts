package org.videolan.podcast

import android.net.Uri
import android.util.Log
import org.videolan.podcast.core.update.UpdateManager
import org.videolan.vlc.extensions.api.VLCExtensionItem
import org.videolan.vlc.extensions.api.VLCExtensionService
import java.util.*

private const val TAG = "PodcastService"

class PodcastService : VLCExtensionService() {


    override fun browse(stringId: String?) {
        if (stringId == null) {
            return

        }
        if (stringId == "") {
            browseSubscriptions()
            return
        }
        PodcastApp.appExecutors.diskIO().execute {
            val dateFormat = android.text.format.DateFormat.getDateFormat(applicationContext)
            val episodes = PodcastApp.database.episodeDao().getAllByPodcast(stringId).sortedByDescending { it.createdAt }
            val podcast = PodcastApp.database.podcastDao().get(stringId)

            val casts = ArrayList<VLCExtensionItem>()

            for (episode in episodes) {
                val item = VLCExtensionItem(episode.link)
                        .setLink(episode.link)
                        .setSubTitle(dateFormat.format(episode.createdAt))
                        .setTitle(episode.title)
                        .setType(if (episode.isAudio) VLCExtensionItem.TYPE_AUDIO else VLCExtensionItem.TYPE_VIDEO)
                try {
                    item.setImageUri(Uri.parse(episode.image))
                } catch (e:NullPointerException) {

                }
                casts.add(item)
            }



            mServiceHandler.post { updateList(podcast.title, stringId, casts, false, false) }


        }
    }

    override fun refresh(parent:String) {
        if (BuildConfig.DEBUG) Log.d(TAG, "Refresh")
        PodcastApp.appExecutors.networkIO().execute {
            val podcastWithEpisodes = PodcastApp.database.podcastsWithEpisodeDao().findPodcastWithEpisodes(parent)
            if (podcastWithEpisodes != null) {

                UpdateManager.updatePodcast(this, podcastWithEpisodes)

            }
            browse(parent)
        }
    }

    override fun progress(id:String, progress:Float) {

        val episode = PodcastApp.database.episodeDao().get(id)
        if (episode != null) {
            episode.progress = progress
            PodcastApp.database.episodeDao().insertOrUpdate(episode)
        }


    }

    override fun readState(id:String, read:Boolean) {
        val episode = PodcastApp.database.episodeDao().get(id)
        if (episode != null) {
            episode.isRead = read
            PodcastApp.database.episodeDao().insertOrUpdate(episode)
        }
    }


    public override fun onInitialize() {
        browseSubscriptions()
    }


    private fun browseSubscriptions() {
        val items = LinkedList<VLCExtensionItem>()

        val allPodcastEntries = PodcastApp.database.podcastDao().listAllValid()
        for ((link, title, _, image, author) in allPodcastEntries) {
            val item = VLCExtensionItem(link)
                    .setSubTitle(author)
                    .setTitle(title)
                    .setType(VLCExtensionItem.TYPE_DIRECTORY)
            if (image != null)
                item.setImageUri(Uri.parse(image))
            items.add(item)

        }


        mServiceHandler.post { updateList("podcasts", "", items, true, false) }
    }

}
